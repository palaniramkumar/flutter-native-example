//https://github.com/flutter/flutter/issues/12731
//https://docs.flutter.io/flutter/services/MethodChannel/invokeMethod.html
//https://flutter.io/platform-channels/#example-client
//https://code.tutsplus.com/tutorials/developing-an-android-app-with-flutter--cms-28270

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State {
  static const platform = const MethodChannel('moonproduct.flutter.io/battery');
  static const platform1 = const MethodChannel('github.com/clovisnicolas/flutter_contacts');

  // Get battery level.
  String _batteryLevel = 'Unknown battery level.';
  String _calllog = "Loading ...";
  String _call = "Loading ...";

  Future<Null> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }

    setState(() {
      _batteryLevel = batteryLevel;
    });
  }

  Future<Null> _getCallLog() async {
    String calllog;
    try {
      //final String result =
      //await platform.invokeMethod('getCallDetails', {"offset": 0});
      /*final String result = await platform.invokeMethod('getCallDetails', {
        "offset": 0,
        "filter": ["+91 95 91 988031", "+91 883 833 2479"]
      });*/
      //final String result = await platform.invokeMethod('getContacts_v2');
      //final String result = await platform.invokeMethod('getProfilePic',{"photo_id":"5619"});
      //final String result = await platform.invokeMethod('getContacts');
      //final String result = await platform.invokeMethod('getContatDetails',{"contact_id":"650"});
      final String result = await platform.invokeMethod('getCallDetails', {"offset": 0,"aggregate":"1"});
      calllog = 'test';
      //calllog = '$result';
    } on PlatformException catch (e) {
      calllog = "Failed to get call log: '${e.message}'.";
    }

    setState(() {
      _calllog = calllog;
    });
  }

  Future<Null> _dial() async {
    String call;
    try {
      //final String result = await platform.invokeMethod('dial_phone_no',{"phone_no":"8056026731","slot":0});
      //final String result = await platform.invokeMethod('getContactDetails',{"contact_id":"918"});
      //final String result = await platform.invokeMethod(
      //'searchContacts', {"search_string": "ram", "type": "name"});
      final String result = await platform.invokeMethod(
          'searchContacts', {"search_string": "95", "type": "number"});
      call = '$result';
    } on PlatformException catch (e) {
      call = "Failed to call : '${e.message}'.";
    }

    setState(() {
      _call = call;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new RaisedButton(
              child: new Text('Get Battery Level'),
              onPressed: _getBatteryLevel,
            ),
            new Text(_batteryLevel),
            new RaisedButton(
              child: new Text('Get Call Log'),
              onPressed: _getCallLog,
            ),
            new Text(_calllog),
            new RaisedButton(
              child: new Text('Dial'),
              onPressed: _dial,
            ),
          ],
        ),
      ),
    );
  }
}

class TipCalculator extends StatelessWidget {
  double billAmount = 0.0;
  double tipPercentage = 0.0;

  @override
  Widget build(BuildContext context) {
    // Create first input field
    TextField billAmountField = new TextField(
        decoration: new InputDecoration(labelText: "Bill amount(\$)"),
        keyboardType: TextInputType.number,
        onChanged: (String value) {
          try {
            billAmount = double.parse(value.toString());
          } catch (exception) {
            billAmount = 0.0;
          }
        });

    // Create another input field
    TextField tipPercentageField = new TextField(
        decoration: new InputDecoration(labelText: "Tip %", hintText: "15"),
        keyboardType: TextInputType.number,
        onChanged: (String value) {
          try {
            tipPercentage = double.parse(value.toString());
          } catch (exception) {
            tipPercentage = 0.0;
          }
        });

    // Create button
    RaisedButton calculateButton = new RaisedButton(
        child: new Text("Calculate"),
        onPressed: () {
          // Calculate tip and total
          double calculatedTip = billAmount * tipPercentage / 100.0;
          double total = billAmount + calculatedTip;

          // Generate dialog
          AlertDialog dialog = new AlertDialog(
              content: new Text("Tip: \$$calculatedTip \n"
                  "Total: \$$total\n"));

          // Show dialog
          showDialog(context: context, child: dialog);
        });
    Container container = new Container(
        padding: const EdgeInsets.all(16.0),
        child: new Column(
            children: [billAmountField, tipPercentageField, calculateButton]));
    AppBar appBar = new AppBar(title: new Text("Tip Calculator"));
    Scaffold scaffold = new Scaffold(appBar: appBar, body: container);
    return scaffold;
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

void main() {
  runApp(new MyApp());
}

void main1() {
  runApp(new MaterialApp(title: 'Tip Calculator', home: new TipCalculator()));
}
