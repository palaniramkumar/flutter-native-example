# Supported APIs

1. getCallDetails
2. getProfilePic
3. dial_phone_no
4. getSimDetails
5. getContacts
6. searchContacts
7. getContactDetails

## getCallDetails

This api is to get the call information of various list of mobile numbers. Aggreagate can be either 0 or 1

* param: 
  - int offset, array filter, int aggregate
  - aggregate can be either 0 or 1

* Usage:
  - await platform.invokeMethod('getCallDetails', {"offset": 0,"aggregate":true});
  - await platform.invokeMethod('getCallDetails', {"offset": 0});
  - await platform.invokeMethod('getCallDetails', {
            "offset": 0,
            "filter": ["+919591988031", "+91 883 833 2479"]
          });
* Response:
    - the call log will be in the json format. 
    - The aggregate will group the results by caller name and only the summary of incoming, outgoing and missed call will be the part of the response.


## getProfilePic

This api is to get profile pic of the requested user. This is the supportive api to the calllog.

* param: 
  - String photo_id

* Usage:
  - await platform.invokeMethod('getProfilePic',{"photo_id":"5619"})
  
* Response:
    - currently we are responding in the base64 format


## dial_phone_no

API to call the user

* param: 
  - String phone_no, int slot

* Usage:
  - await platform.invokeMethod('dial_phone_no',{"phone_no":"8056026731","slot":0});
  
* Response:
    - NA
    

## getSimDetails

API to available sim slots

* param: 
  - None

* Usage:
  - await platform.invokeMethod('getSimDetails');
  
* Response:
    - SIM slot details
    
## getContacts

API to get contact lists

* param: 
  - None

* Usage:
  - await platform.invokeMethod('getContacts');
  
* Response:
    - contacts list in the json format

## getContacts_v2

API to get contact lists (fork of https://github.com/clovisnicolas/flutter_contacts)

* param: 
  - None

* Usage:
  - await platform.invokeMethod('getContacts_v2');
  
* Response:
    - contacts list in the arraylist format


## searchContacts

search API to filter the contact details

* param: 
  - string search_string, string type

* Usage:
  - await platform.invokeMethod('searchContacts', {"search_string": "95", "type": "number"});
  - await platform.invokeMethod('searchContacts', {"search_string": "ram", "type": "name"})
  
* Response:
    - search results list in the json format
    
## getContactDetails

get contact details like email, mobile number etc

* param: 
  - string contact_id

* Usage:
  - await platform.invokeMethod('getContactDetails',{"contact_id":"918"})
  
* Response:
    - contact details list in the json format