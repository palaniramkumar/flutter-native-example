package com.moonproduct.phonex;

import android.os.Bundle;


import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;


import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;


import com.moonproduct.helper.ContactHelper;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "moonproduct.flutter.io/battery";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final DialPhone dial = new DialPhone(this);
        final MyCallLog mycalllog = new MyCallLog(this);
        final ContactHelper contacts  = new ContactHelper(this);



        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                new MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, Result result) {

                        if (call.method.equals("getCallDetails")) {
                            int offset = call.argument("offset");
                            ArrayList  filter = call.argument("filter");

                            if(filter !=null){
                                String tmp=null;
                                for(int i=0;i<filter.size();i++) {
                                    tmp = filter.get(i).toString().replaceAll(" ", "");
                                    filter.set(i, tmp);
                                }
                            }

                            String aggregate = call.argument("aggregate");
                            JSONArray calllog = null;
                            if(filter == null && aggregate == null)
                                calllog = mycalllog.getCallDetails(offset);
                            else if(filter != null && aggregate == null)
                                calllog = mycalllog.getCallDetails(offset,filter);

                            else if(aggregate.equals("1"))
                                calllog = mycalllog.getCallDetails(offset,filter,true);
                            else if(aggregate.equals("0"))
                                calllog = mycalllog.getCallDetails(offset,filter,false);

                            result.success(calllog.toString());

                        }
                        else if (call.method.equals("getProfilePic")) {
                            String photo_id = call.argument("photo_id");
                            byte[] b64Image= mycalllog.getProfilePic(photo_id);
                            result.success(b64Image);

                        }
                        else if (call.method.equals("dial_phone_no")) {
                            String phone_no = call.argument("phone_no");
                            int slot = call.argument("slot");


                            dial.dial_phone_no(phone_no,slot);
                            result.success("Success");

                        }
                        else if (call.method.equals("getSimDetails")) {
                            JSONArray sim_details = mycalllog.getSimDetails();
                            result.success(sim_details.toString());

                        }
                        else if (call.method.equals("getContacts")) {
                            JSONArray response = contacts.getContacts();
                            result.success(response.toString());

                        }
                        else if (call.method.equals("getContacts_v2")) {
                            ArrayList response = contacts.getContacts_v2(null);
                            result.success(response);

                        }
                        else if (call.method.equals("searchContacts")) {
                            String search_string = call.argument("search_string");
                            String type = call.argument("type");
                            JSONArray response = contacts.getContacts(search_string,type);
                            result.success(response.toString());


                        }
                        else if (call.method.equals("getContactDetails")) {
                            String contact_id = call.argument("contact_id");
                            System.out.println(contact_id);
                            JSONArray response = contacts.getContactDetails(contact_id);
                            result.success(response.toString());

                        }
                        else {
                            result.notImplemented();
                        }
                    }
                });
    }



}


