package com.moonproduct.phonex;

import android.app.Activity;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

// Reference - http://www.zeustechnocrats.com/android-read-contact-and-display-in-listview/
public class ContactHelper {
    private Activity activity;
    public ContactHelper(Activity activity){
        this.activity = activity;
    }

    public JSONArray getContacts(){
        JSONArray array = new JSONArray();
        Cursor cursor = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null,null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        while (cursor.moveToNext()) {
            JSONObject item = new JSONObject();
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String contact_Id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            try {
                item.put("name", name);
                item.put("phone_number", phoneNumber);
                item.put("contact_id", contact_Id);
                Log.d("calllog", name);
                array.put(item);
            }
            catch (Exception e){}
        }
        System.out.println(array);
        return array;

    }
    public JSONArray getContactDetails(String contact_Id){
        System.out.println(contact_Id);
        Cursor cursor_phone = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, ContactsContract.Contacts._ID + " = " + contact_Id,
                null, null);
        JSONArray array = new JSONArray();

        while (cursor_phone.moveToNext()) {
            JSONObject item = new JSONObject();
            String phNumber = cursor_phone.getString(cursor_phone
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String phNumber_type= "";
            int PHONE_TYPE =cursor_phone.getInt(cursor_phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
            int PHONE_PRMARY =cursor_phone.getInt(cursor_phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.IS_PRIMARY));
            switch (PHONE_TYPE) {
                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                    // home number
                    phNumber_type = "Home";
                    break;
                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                    // mobile number
                    phNumber_type = "Mobile";
                    break;
                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                    // work(office) number
                    phNumber_type = "Work";
                    break;


            }
            try {
                item.put("phone_number", phNumber);
                item.put("phone_type", phNumber_type);
                item.put("primary", PHONE_PRMARY);
                Log.d("calllog", phNumber_type);
                array.put(item);
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
        System.out.println(array);

        return array;
    }
}
