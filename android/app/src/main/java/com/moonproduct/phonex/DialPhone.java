package com.moonproduct.phonex;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;

import java.util.List;
import android.support.v4.app.ActivityCompat;
import  android.support.v4.content.ContextCompat;
import io.flutter.app.FlutterActivity;

public class DialPhone  {
    private final static String simSlotName[] = {
            "extra_asus_dial_use_dualsim",
            "com.android.phone.extra.slot",
            "slot",
            "simslot",
            "sim_slot",
            "subscription",
            "Subscription",
            "phone",
            "com.android.phone.DialingMode",
            "simSlot",
            "slot_id",
            "simId",
            "simnum",
            "phone_type",
            "slotId",
            "slotIdx"
    };
    private Activity activity;
    public DialPhone(Activity activity){
        this.activity = activity;
    }

    protected void dial_phone_no(String phone_no,int simselected){

        /*
            Reference: https://stackoverflow.com/questions/25524476/make-call-using-a-specified-sim-in-a-dual-sim-device
        */

        int permissionCheck = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    new String[]{Manifest.permission.CALL_PHONE},
                    123);
        } else {
            Intent intent = new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + phone_no));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("com.android.phone.force.slot", true);
            intent.putExtra("Cdma_Supp", true);

            TelecomManager telecomManager = (TelecomManager) activity.getSystemService(Context.TELECOM_SERVICE);
            List<PhoneAccountHandle> phoneAccountHandleList = telecomManager.getCallCapablePhoneAccounts();

            if (simselected== 0) {   //0 for sim1
                for (String s : simSlotName)
                    intent.putExtra(s, 0); //0 or 1 according to sim.......

                if (phoneAccountHandleList != null && phoneAccountHandleList.size() > 0)
                    intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList.get(0));

            } else {      //1 for sim2
                for (String s : simSlotName)
                    intent.putExtra(s, 1); //0 or 1 according to sim.......

                if (phoneAccountHandleList != null && phoneAccountHandleList.size() > 1)
                    intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList.get(1));

            }
            activity.startActivity(intent);
        }

    }
}
