package com.moonproduct.phonex;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Base64;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import com.moonproduct.helper.*;


public class MyCallLog {
    private Activity activity;
    static String TAG = "calllog";

    public MyCallLog(Activity activity){
        this.activity = activity;
    }

    protected JSONArray getSimDetails(){
        JSONArray array = new JSONArray();
        SubscriptionManager subManager = (SubscriptionManager) activity.getSystemService(activity.TELEPHONY_SUBSCRIPTION_SERVICE);
        List<SubscriptionInfo> subInfoList = subManager.getActiveSubscriptionInfoList();

        for(int i = 0; i < subInfoList.size(); i++ ) {
            JSONObject item = new JSONObject();
            int subID = subInfoList.get(i).getSubscriptionId();
            int simPosition = subInfoList.get(i).getSimSlotIndex();
            String carrier_name = subInfoList.get(i).getCarrierName().toString();
            String mobile_number = subInfoList.get(i).getNumber();

            try {
                item.put("subID", subID);
                item.put("simPosition", simPosition);
                item.put("carrier_name", carrier_name);
                item.put("mobile_number", mobile_number);

                if (subManager.isNetworkRoaming(subID)) {
                    item.put("Network", "ROAMING");
                    Log.d("TEST", "Simcard in slot " + simPosition + " has subID == " + subID + " and it is in ROAMING");
                } else {
                    item.put("Network", "HOME");
                    Log.d("TEST", "Simcard in slot " + simPosition + " has subID == " + subID + " and it is HOME");
                }
                array.put(item);
            }

            catch(JSONException e){

            }
        }
        return array;
    }
    public static int getSimIdColumn(final Cursor c) {

        for (String s : new String[] { "sim_id", "simid", "sub_id","subscription_id" }) {
            int id = c.getColumnIndex(s);
            if (id >= 0) {
                Log.d(TAG, "sim_id column found: " + s);
                return id;
            }
        }
        Log.d(TAG, "no sim_id column found");
        return -1;
    }
    protected  byte[] getProfilePic(String photo_id) {
        if(photo_id.equalsIgnoreCase("0"))
            return null;

        Cursor c = this.activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{
                ContactsContract.CommonDataKinds.Photo.PHOTO
        }, ContactsContract.Data._ID + "=?", new String[]{
                photo_id
        }, null);
        byte[] imageBytes = null;
        if (c != null) {
            if (c.moveToFirst()) {
                imageBytes = c.getBlob(0);
            }
            c.close();
        }

        return imageBytes;
    }
    protected JSONArray getCallDetails(int offset) {
        return new CallLogHelper(this.activity).getCalls(offset);
    }
    protected JSONArray getCallDetails(int offset,ArrayList filter) {
        return new CallLogHelper(this.activity).getCalls(offset,filter);
    }
    protected JSONArray getCallDetails(int offset,ArrayList filter,boolean aggregate) {
        return new CallLogHelper(this.activity).getCalls(offset,filter,aggregate);
    }


}
