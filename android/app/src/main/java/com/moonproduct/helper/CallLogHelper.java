package com.moonproduct.helper;
import java.text.SimpleDateFormat;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
class CallUsage {
    String name,contact_lookup_uri;
    int missed,out_going,incoming,total_duration;
    String last_call_date,photo_id;

    public CallUsage(String name,String number, String contact_lookup_uri,  int callType,  int callDuration, Date date, String photo_id) {
        if (name== null)
            this.name = number;
        else
            this.name=name;
        this.contact_lookup_uri = contact_lookup_uri;
        if (CallLog.Calls.MISSED_TYPE == callType)
            this.missed = 1;
        if (CallLog.Calls.OUTGOING_TYPE == callType)
            this.out_going = 1;
        if (CallLog.Calls.INCOMING_TYPE == callType)
            this.incoming = 1;
        this.total_duration = callDuration;
        this.last_call_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
        this.photo_id = photo_id;

    }
}

class CallAggregator{
    List <CallUsage> call_summary;
    public CallAggregator(){
        call_summary = new LinkedList<CallUsage>();
    }
    private int has_entry(CallUsage log){
        System.out.println("log.contact_lookup_uri = "+log.contact_lookup_uri);
        for (int i=0;i<call_summary.size();i++){
            if(call_summary.get(i).contact_lookup_uri!=null && call_summary.get(i).contact_lookup_uri.equals(log.contact_lookup_uri))
                return i;
        }
        return -1;
    }
    public void add(CallUsage log){
        int pos = has_entry(log);
        if(pos ==-1)
            call_summary.add(log);
        else{
            CallUsage item = call_summary.get(pos);
            item.incoming += log.incoming;
            item.missed += log.missed;
            item.out_going += log.out_going;
            item.total_duration += log.total_duration;
        }
    }
}

public class CallLogHelper {
    private Activity activity;
    static String TAG = "CallLogHelper";

    public CallLogHelper(Activity activity) {
        this.activity = activity;
    }
    protected JSONArray getSimDetails(){
        JSONArray array = new JSONArray();
        SubscriptionManager subManager = (SubscriptionManager) activity.getSystemService(activity.TELEPHONY_SUBSCRIPTION_SERVICE);
        List<SubscriptionInfo> subInfoList = subManager.getActiveSubscriptionInfoList();

        for(int i = 0; i < subInfoList.size(); i++ ) {
            JSONObject item = new JSONObject();
            int subID = subInfoList.get(i).getSubscriptionId();
            int simPosition = subInfoList.get(i).getSimSlotIndex();
            String carrier_name = subInfoList.get(i).getCarrierName().toString();
            String mobile_number = subInfoList.get(i).getNumber();

            try {
                item.put("subID", subID);
                item.put("simPosition", simPosition);
                item.put("carrier_name", carrier_name);
                item.put("mobile_number", mobile_number);

                if (subManager.isNetworkRoaming(subID)) {
                    item.put("Network", "ROAMING");
                    Log.d("TEST", "Simcard in slot " + simPosition + " has subID == " + subID + " and it is in ROAMING");
                } else {
                    item.put("Network", "HOME");
                    Log.d("TEST", "Simcard in slot " + simPosition + " has subID == " + subID + " and it is HOME");
                }
                array.put(item);
            }

            catch(JSONException e){

            }
        }
        return array;
    }
    public static int getSimIdColumn(final Cursor c) {

        for (String s : new String[] { "sim_id", "simid", "sub_id","subscription_id" }) {
            int id = c.getColumnIndex(s);
            if (id >= 0) {
                Log.d(TAG, "sim_id column found: " + s);
                return id;
            }
        }
        Log.d(TAG, "no sim_id column found");
        return -1;
    }


    public JSONArray getCalls(int offset) {
        return getCalls(offset,null,false);
    }
    public JSONArray getCalls(int offset,boolean aggragate) {
        return getCalls(offset,null,aggragate);
    }
    public JSONArray getCalls(int offset,ArrayList numbers) {
        return getCalls(offset,numbers,false);
    }
    public JSONArray getCalls(int offset,ArrayList numbers,boolean aggragate) {
        System.out.println("Aggregator: "+aggragate);
        final int LIMIT = 20;
        CallAggregator aggregator = new CallAggregator();
        String selection = null;
        if (numbers != null) {
            selection ="";
            for (int i=0;i<numbers.size();i++) {
                selection += CallLog.Calls.NUMBER + " = '" + numbers.get(i)+"'";
                        if(i+1 != numbers.size())
                            selection += " or ";
            }
        }
        JSONArray array = new JSONArray();
        Uri contacts = CallLog.Calls.CONTENT_URI;
        boolean s_bSamsung = false;
        String manufacturer = android.os.Build.MANUFACTURER;

        if(manufacturer.equalsIgnoreCase("samsung")  )
            s_bSamsung = true;

        if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT && s_bSamsung){
            contacts = Uri.parse("content://logs/call");
        }
        Cursor managedCursor = activity.getContentResolver().query(contacts,
                null,
                selection,
                null,
                "DATE DESC LIMIT "+LIMIT+" OFFSET "+(offset*LIMIT));

        /*Cursor managedCursor = activity.managedQuery(CallLog.Calls.CONTENT_URI, null,
                null, null, "DATE DESC");*/
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int name=managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);// for name
        int photo= managedCursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_ID);
        int contact_lookup_uri= managedCursor.getColumnIndex(CallLog.Calls.CACHED_LOOKUP_URI);

        //int sim = managedCursor.getColumnIndex(CallLog.Calls.PHONE_ACCOUNT_ID);
        int sim = getSimIdColumn(managedCursor);

        while (managedCursor.moveToNext()) {


            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            String cache_name = managedCursor.getString(name);
            String photo_id = "";
            String contact_lookup = managedCursor.getString(contact_lookup_uri);;
            try{
                photo_id = managedCursor.getString(photo);

            }
            catch(Exception e){
                System.out.println(e.toString());
            }
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            int sim_id = 0;
            if(sim != -1)
                sim_id = managedCursor.getInt(sim);

            //String dir = null;
            int dircode = Integer.parseInt(callType);

            if(aggragate == true){
                CallUsage log = new CallUsage(cache_name,phNumber,contact_lookup,dircode,Integer.parseInt(callDuration),callDayTime,photo_id);
                aggregator.add(log);
            }
            else {
                JSONObject item = new JSONObject();
                try {
                    item.put("phone_number", phNumber);
                    item.put("call_type", dircode);
                    item.put("duration", callDuration);
                    item.put("date", callDayTime);
                    item.put("cache_name", cache_name);
                    item.put("photo_id", photo_id);

                    item.put("sim", sim_id);
                    Log.d("calllog", sim_id + "");
                    array.put(item);

                } catch (JSONException e) {

                }
            }

        }
        managedCursor.close();
        if(aggragate == true) {
            for (int i = 0; i < aggregator.call_summary.size(); i++) {
                JSONObject item = new JSONObject();
                try {
                    String contact_uri = aggregator.call_summary.get(i).contact_lookup_uri;
                    item.put("incoming", aggregator.call_summary.get(i).incoming);
                    item.put("out_going", aggregator.call_summary.get(i).out_going);
                    item.put("missed", aggregator.call_summary.get(i).missed);
                    item.put("last_call_date", aggregator.call_summary.get(i).last_call_date);
                    item.put("cache_name", aggregator.call_summary.get(i).name);
                    item.put("total_duration", aggregator.call_summary.get(i).total_duration);
                    item.put("photo_id", aggregator.call_summary.get(i).photo_id);
                    item.put("contact_lookup_uri",contact_uri );

                    String[] projection = new String[] {  ContactsContract.Contacts.LOOKUP_KEY};
                    if(contact_uri !=null) {
                        Cursor lookup_key_curser = activity.getContentResolver().query(Uri.parse(contact_uri), projection, null, null, null);
                        if (lookup_key_curser != null && lookup_key_curser.moveToNext()) {
                            String lookup_key = lookup_key_curser.getString(0);
                            Cursor cursor = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY + " = ?", new String[] { lookup_key }, null);
                            try
                            {
                                List<String> mobile_list = new LinkedList<>();
                                while (cursor.moveToNext())
                                {
                                    String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    //int phone_type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                    //CharSequence phoneLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(activity.getResources(), type, "Undefined");
                                    // boolean isPrimary = (cursor.getInt(cursor.getColumnIndex(Phone.IS_PRIMARY)) == 1);
                                    phoneNumber = phoneNumber.replaceAll(" ","");
                                    if(!mobile_list.contains(phoneNumber))
                                        mobile_list.add(phoneNumber); //String.join(",",mobile_list)

                                }
                                if(!mobile_list.isEmpty())
                                    item.put("phones", TextUtils.join(",",mobile_list));
                                else
                                    item.put("phones", null);
                            } finally
                            {
                                cursor.close();
                                lookup_key_curser.close();
                            }
                        }



                    }
                    else {
                        String [] phone =  {aggregator.call_summary.get(i).name};
                        item.put("phones", phone);
                    }
                    array.put(item);
                    System.out.println(item);

                } catch (JSONException e) {

                }

            }
        }
        Log.d(TAG, "Response:" + array);
        return array;

    }

}